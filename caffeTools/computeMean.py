from pycocotools.coco import COCO
import os
from skimage import io
import numpy as np
import argparse
import cv2


def get_arguments():
    # Import arguments
    parser = argparse.ArgumentParser()
    
    # Mandatory arguments
    parser.add_argument('folder', type=str, help='\
    Path to folder with labels.')
    
    # Optional arguments
    # TODO: merge with kim's file for dealing with JSON
    parser.add_argument('--type', default='.jpg', help='\
    Extension of the labels')
    
    return parser.parse_args()

if __name__ == '__main__':

    # Get all options
    args = get_arguments()
    
    # Init R, G, B
    Rsum = Gsum = Bsum = 0
    cpt = 0
    
    # For each image
    for img in os.listdir(args.folder):
        # New img
        cpt = cpt+1
        print "Will compute img", cpt
        
        # Open image
        image = cv2.imread(os.path.join(args.folder, img))
        
        # If there is no color channels, resize the matrix
        if len(image.shape) is 2:
            image = np.resize(image, (image.shape[0],image.shape[1],3))
        
        # Get the R, G, B values
        npixel = image.shape[0]*image.shape[1]
        image = np.reshape(image,(npixel,3))
        image = np.ndarray.sum(image,axis=0)/npixel
        Rsum += image[0]
        Gsum += image[1]
        Bsum += image[2]

    
    print cpt, "images mean = ", Rsum/cpt, ", ", Gsum/cpt, ", ", Bsum/cpt

