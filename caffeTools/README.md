# Tools for Caffe
Tools to train our network and visualize results. It also has the utilities to
create dataset (conversion into a readable format for Caffe).

## Main scripts
### trainNetwork.py
A script to train Caffe networks. This replaces the ‘solve.py’ scripts used by
most of FCN architectures from the original Berkeley github repos.  
While using this, be aware that many of the values specified in Caffe's
solver.prototxt which are ignored by the original solve.py are *not* ignored by trainNetwork.py. test_interval, test_iter and max_iter are the most notable
examples of this, so, be sure that you have set these properly in your
solver.prototxt file. Also please be aware of the behaviour of your chosen data
layer - the statistics computed during the test phase will depend on its
behaviour, particularly if your test_iter value is bigger than the number of
samples in your test set.

### segment.py
A script to test semantic-segmentation networks, both individually and as
ensembles, by running them on a provided dataset and computing metrics on the
results. As input it takes a .prototxt config file - the format of this config
file is defined in ensemble.proto, and an example is provided in
test_example.prototxt.

### display_loss.py
A script for plotting the loss, mean IU, and other statistics contained in the
training logfiles recorded from Caffe’s output.

### get_class_weights.py
In SegNet, the loss layer weights each class by its frequency in the dataset.
This script computes the class frequencies of a dataset so that they can be
used to run SegNet on those datasets.

### view_weights.py
Visualize the weights of a layer from the caffemodel of a given iteration.

### create_dataset_lmdb.py
Create a dataset given a directory with the images, the number of class we
want, the image format etc.

### configure
A simple bash script that auto-generates protocol-buffer python files from
.proto message definition files. When you first check out this repo, this
script will need to be run before you can use some of the other scripts that
rely on it. You’ll also need to run it again if you make changes to any .proto
files in this repo.


### Not stable, should be checked

- `copy_weights.py`: Converts VGG into a fully convolutional network (by 
  copying the fc6 and fc7 fully convolutional layers weights)

- `plot_losses`: Very basic tools to read / plot losses
