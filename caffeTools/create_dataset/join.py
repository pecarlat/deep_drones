#!/usr/bin/env python

"""Divide an image into few sections."""

import os
import cv2
import numpy as np
import argparse
from PIL import Image


def getArguments():
    """Defines and parses command-line arguments to this script."""
    parser = argparse.ArgumentParser()
    
    # Required arguments
    parser.add_argument('input', help='\
    Folder with the input images.')
    parser.add_argument('output', help='\
    Folder with the input images.')
    parser.add_argument('nb_crop', type=int, help='\
    Folder with the input images.')
    
    return parser.parse_args()
    

if __name__ == "__main__":
    
    # Get the arguments
    args = getArguments()
    
    # For each image in the input directory
    for _i, i in enumerate(os.listdir(args.output)):
        
        # Get the image
        print 'Image', i, 'in process'
        orig_img = []
        for crop in range(args.nb_crop*args.nb_crop):
            imgName = os.path.splitext(os.path.basename(i))[0] + '_' + str(crop) + '.png'
            orig_img.append(cv2.imread(os.path.join(args.input, imgName)))
        shp = orig_img[0].shape
        
        new_im = np.zeros((shp[0]*args.nb_crop,shp[1]*args.nb_crop,3), np.uint8)
        
        for h in range(0, args.nb_crop):
            for w in xrange(0, args.nb_crop):
                new_im[h*shp[0]:(h+1)*shp[0], w*shp[1]:(w+1)*shp[1]] = orig_img[h*args.nb_crop+w]
        
        # Save image(s)
        cv2.imwrite(os.path.join(args.output, i), new_im)


