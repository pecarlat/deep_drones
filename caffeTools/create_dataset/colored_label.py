# Convert each label files from colours to number of classes
# sudo python convert_labels.py /home/shared/datasets/VOCdevkit/VOC2012/SegmentationClass /home/shared/datasets/VOCdevkit/VOC2012/colours/pascal_voc_21_colours.png /home/shared/datasets/VOCdevkit/VOC2012/SegmentationClass_2/


import lmdb
import argparse
import numpy as np
import png
import cv2
import sys
import os
from PIL import Image
import numpngw
import scipy.misc



def get_arguments():
    # Import arguments
    parser = argparse.ArgumentParser()
    
    # Mandatory options
    parser.add_argument('img_input', type=str, \
                                    help='Path to the img to convert')
    parser.add_argument('path_to_transition', type=str, \
                                    help='File with the transition matrix (color.png or transition.png) ; created by create_transition.py')
    parser.add_argument('img_output', type=str, \
                                    help='Path where to create the new labels')
    
    return parser.parse_args()



def main(args):
    
    # Get all options
    args = get_arguments()
    
    # Get the colours
    colours = cv2.imread(args.path_to_transition)[0]
    
    # Get the image and create a copy
    im = cv2.imread(args.img_input)
    newImage = np.zeros_like(im)
    print type(newImage)
    
    # Replace each pixel of the copy by the number of the class
    for i in range(0, im.shape[0]):
        for j in range(0, im.shape[1]):
            col = colours[im[i,j,0]]
            newImage[i,j,2] = col[0]
            newImage[i,j,1] = col[1]
            newImage[i,j,0] = col[2]
    
    # Save the new label
    print type(newImage)
    print newImage.shape
    newImage_ = Image.fromarray(newImage)
    newImage_.save(args.img_output)



if __name__ == '__main__':
    
    main(None)
    
    pass


