# Resize images in the folder input_path


import numpy as np
import glob
from random import shuffle
import cv2
import os.path
import argparse
from PIL import Image


CROP_X = 400
CROP_Y = 400
ITER = 200


def get_arguments():
    # Import arguments
    parser = argparse.ArgumentParser()
    
    parser.add_argument('dataset', type=str, \
                                    help=   'Path to the folder with all images to resize')
    parser.add_argument('input_list', type=str, \
                                    help=   'Path to the folder with all images to resize')
    parser.add_argument('output_img', type=str, \
                                    help=   'Path to output folder')
    parser.add_argument('output_seg', type=str, \
                                    help=   'Path to output folder')
    return parser.parse_args()


def do_random_crop(image, segmentation):
    randX = np.random.choice(image.shape[0]-CROP_X)
    randY = np.random.choice(image.shape[1]-CROP_Y)
    
    img = image[randX:randX+CROP_X, randY:randY+CROP_Y]
    seg = segmentation[randX:randX+CROP_X, randY:randY+CROP_Y]
    
    return [img, seg]


def main(args):
     # Get all options
    args = get_arguments()
    
    # Read txt
    lines = [line.rstrip('\n') for line in open(args.input_list)]
    
    # For each image
    for idx, l in enumerate(lines):
        
        # load image:
        lSp = l.split()
        full_path_img = os.path.join(args.dataset, lSp[0])
        full_path_seg = os.path.join(args.dataset, lSp[1])
        img = cv2.imread(full_path_img)
        seg = cv2.imread(full_path_seg)
        
        assert CROP_X <= img.shape[0]
        assert CROP_Y <= img.shape[1]
        
        for i in range(ITER):
            cropped = do_random_crop(img, seg)
            
            output_img = os.path.join(args.output_img, lSp[0].split('/', 1)[1])
            output_img = output_img[:len(output_img)-4] + "_" + str(i) + output_img[len(output_img)-4:]
            cv2.imwrite(output_img, cropped[0])
            
            output_seg = os.path.join(args.output_seg, lSp[1].split('/', 1)[1])
            output_seg = output_seg[:len(output_seg)-4] + "_" + str(i) + output_seg[len(output_seg)-4:]
            cv2.imwrite(output_seg, cropped[1])
            
            print output_img.split(args.dataset)[1], output_seg.split(args.dataset)[1]


if __name__ == '__main__':
    
    main(None)
    
    pass


