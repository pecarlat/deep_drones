# FCN layers for Caffe
These python caffe layers are provided, unmodified, from the Github repository
located at https://github.com/shelhamer/fcn.berkeleyvision.org/.  
They are provided so that we can train the fcn networks of this repo.

data_layers.py is the exception - it is based on Shelhamer's code but heavily
modified to reduce code duplication and allow new dataset types.