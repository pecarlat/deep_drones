# Utils for the framework Caffe
This module represents a collection of utility classes and functions used
across several of our repositories.  
It is recommended to add the directory of this repo to your PYTHONPATH
environment variable:
```bash
export PYTHONPATH=/path/to/caffeUtils:$PYTHONPATH
```

## fcnLayers
This directory contains various python Caffe layers used to read in different
types of data for training and testing. Check out their own README.md for
details.

## FCN-8s
The prototxts and solvers to add in Caffe to train the networks

## VGG
The prototxts to add in Caffe to deploy the networks


## Main files
- solve.py: module containing methods for training and testing Caffe networks
with Python.
- score.py: module containing utility methods for computing the metrics we use
to judge the quality of a semantic segmentation, such as pixel accuracy and
mean IU.
- protoUtils.py: module containing utility methods for interacting with
protocol buffer objects.
