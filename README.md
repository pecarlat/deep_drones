# Deep drones
Code used in my internship for the deep drone project, at NII (National
Institute of Informatics).

## Problematic
We'd like to train a model to do semantic segmentation on aerial views (from
drones). Currently, a couple of algorithms exist, like SegNet and FCN-8s. Both
perform well on standard datasets (CamVid, Pascal VOC, ...) but are really bad
on real situations, like our own datasets.  
In order to fix this, we've:
- built our own model, an extension of the FCN-xx network, combined with
residual connections, called FCN-xx-ResNet-xx
- an ensemble of our best models in order to improve the global accuracy
- reduced the size of our ensemble using a distillation method to let it work
locally on a physical drone in real time

The final performance is pretty good, as we can see in the following video:
[![Problematic](https://img.youtube.com/vi/Mj1Gj0HmP6A/0.jpg)](https://www.youtube.com/watch?v=Mj1Gj0HmP6A)


## FCN-xx-ResNet-xx
We proposed two variants: one with the upsampling method of FCN-32 and another
one with the FCN-16 method (as shown beloz). The figure is done with the
ResNet-xx architecture to improve the visibility, but we created the three
variants (50, 101, and 152 layers) for both upsampling methods (FCN-16,
FCN-32).
![FCN-16-ResNet-50](resources/model.png)


## Code
We used the Caffe framework for our experiments:
- CafeUtils: modules to build our models in Caffe, and to compute evaluation
metrics
- CafeTools: tools to train our models, create the datasets, visualize the
results
- transferLearningFramework: framework to manipulate our models, perform
transfer learning, ensembles, and distillation

## Datasets
In order to test our project, we've been using a couple of standard datasets to
validate our models, and also two datasets, called Okutama and Swiss, which
we've built by ourselves, using aerial views of our drones. These datasets
aren't available, but you can contact me or any of the contributor to get them
if you need them for research purposes.

### Handmade datasets
Okutama dataset                     |  Swiss dataset
:----------------------------------:|:----------------------------------:
![](resources/okutama_example.jpg)  |  ![](resources/swiss_example.jpg)

### Standard datasets
![Comparison](resources/datasets_comparison.jpg)
